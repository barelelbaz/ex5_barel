#include "Point.h"

Point::Point()
{
	this->_x = 0;
	this->_y = 0;
}

Point::Point(double x, double y)
{
	this->_x = x;
	this->_y = y;
}

Point::Point(const Point & other)
{
	this->_x = other._x;
	this->_y = other._y;
}

Point::~Point()
{
	this->_x = 0;
	this->_y = 0;
}

Point Point::operator+(const Point & other) const
{
	Point p;
	p._x = this->_x + other._x;
	p._y = this->_y + other._y;
	return p;
}

Point & Point::operator+=(const Point & other)
{
	this->_x += other._x;
	this->_y += other._y;
	return *this;
}

double Point::getX() const
{
	return this->_x;
}

double Point::getY() const
{
	return this->_y;
}

void Point::setX(const double x)
{
	this->_x = x;
}

void Point::setY(const double y)
{
	this->_y = y;
}

double Point::distance(const Point & other) const
{
	return sqrt(pow(this->_y - other._y, 2) - pow(this->_x - other._x, 2));
}
