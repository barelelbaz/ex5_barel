#pragma once
#include "Polygon.h"

class Triangle : public Polygon
{
private:
	Point _top;
	Point _bottomLeft;
	Point _bottomRight;
public:
	void draw(cimg_library::CImgDisplay & disp, cimg_library::CImg<unsigned char>& board);
	void clearDraw(cimg_library::CImgDisplay & disp, cimg_library::CImg<unsigned char>& board);
	Triangle(const Point& a, const Point& b, const Point& c, const string& type, const string& name);
	virtual ~Triangle();
	
	virtual double getArea() const;
	virtual double getPerimeter() const;
	virtual void move(const Point& other);
	// override functions if need (virtual + pure virtual)
};