#include "Triangle.h"



void Triangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char GREEN[] = { 0, 255, 0 };
	board.draw_triangle(_top.getX(), _top.getY(),
		_bottomLeft.getX(), _bottomLeft.getY(),
		_bottomRight.getX(), _bottomRight.getY(), GREEN, 100.0f).display(disp);
}

void Triangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	board.draw_triangle(_top.getX(), _top.getY(),
		_bottomLeft.getX(), _bottomLeft.getY(),
		_bottomRight.getX(), _bottomRight.getY(), BLACK, 100.0f).display(disp);
}

Triangle::Triangle(const Point & a, const Point & b, const Point & c, const string & type, const string & name): Polygon(type, name)
{
	this->_top = a;
	this->_bottomLeft = b;
	this->_bottomRight = c;
}

Triangle::~Triangle()
{
}

 double Triangle::getArea() const//calac area of any triangle by haron formula
{
	double a, b, c, s, area;
	a = this->_top.distance(this->_bottomLeft);
	b = this->_bottomLeft.distance(this->_bottomRight);
	c = this->_bottomRight.distance(this->_top);
	s = (a + b + c) / 2;
	area = sqrt(s * (s - a)* (s - b) *(s - c));
	return area;
}

double Triangle::getPerimeter() const
{
	double a, b, c;
	a = this->_top.distance(this->_bottomLeft);
	b = this->_bottomLeft.distance(this->_bottomRight);
	c = this->_bottomRight.distance(this->_top);
	return a + b + c;
}

void Triangle::move(const Point & other)
{
	this->_top += other;
	this->_bottomLeft += other;
	this->_bottomRight += other;
}
