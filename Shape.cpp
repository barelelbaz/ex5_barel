#include "Shape.h"

Shape::Shape()
{
	this->_name = "";
	this->_type = "";
}

Shape::Shape(const string & name, const string & type)
{
	this->_name = name;
	this->_type = type;
}

void Shape::printDetails() const
{
	printf("%s\t\t%s\t\t%s\t\t%s\t\t", this->_type, this->_name,this->getArea(), getPerimeter());
}

string Shape::getType() const
{
	return this->_type;
}

string Shape::getName() const
{
	return this->_name;
}

void Shape::setType(const string & type)
{
	this->_type = type;
}

void Shape::setName(const string & name)
{
	this->_name = name;
}
