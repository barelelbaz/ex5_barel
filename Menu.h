#pragma once
#include "Circle.h"
#include "Arrow.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "CImg.h"
#include <vector>
#include <iostream>
#include <sstream>


class Menu
{
public:

	Menu();
	~Menu();
	unsigned int displayChoicesMenu();
	enum Shapes { circle, arrow, triangle, rectangle };
	enum MainMenu { add_shape_choice, modify_shape_choice, delete_all_shapes, exit_choice };
	enum ModifyShapeMenu { move_shape, get_details, remove_shape };

	int printShapesMenu();
	void addCircle(vector<Shape*>& shapes);
	void addArrow(vector<Shape*>& shapes);
	void addTriangle(vector<Shape*>& shapes);
	void addRectangle(vector<Shape*>& shapes);
	void addShape(vector<Shape*>& shapes);
	void showAllShapes(vector<Shape*>& shapes);
	void showShapeMenu(vector<Shape*>& shapes, unsigned int index);
	void moveShapeMenu(vector<Shape*>& shapes, unsigned int index);
	void removeShape(vector<Shape*>& shapes, unsigned short index, bool delete_from_shapes);
	void deleteAllShapes(vector<Shape*>& shapes);
private: 

	cimg_library::CImg<unsigned char>* _board;
	cimg_library::CImgDisplay* _disp;
};

