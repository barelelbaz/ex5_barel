#pragma once
#include <vector>
#include <math.h>
//#include "CImg.h"

class Point
{
private:
	double _x;
	double _y;
public:
	Point();
	Point(double x, double y);
	Point(const Point& other);
	virtual ~Point();
	
	Point operator+(const Point& other) const;
	Point& operator+=(const Point& other);

	double getX() const;
	double getY() const;
	void setX(const double x);
	void setY(const double y);

	double distance(const Point& other) const;

	
};