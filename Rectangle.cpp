#include "Rectangle.h"




void myShapes::Rectangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char WHITE[] = { 255, 255, 255 };
	board.draw_rectangle(_topCornerLeft.getX(), _topCornerLeft.getY(),
		_topCornerRight.getX(), _topCornerRight.getY(), WHITE, 100.0f).display(disp);
}

void myShapes::Rectangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0};
	board.draw_rectangle(_topCornerLeft.getX(), _topCornerLeft.getY(),
		_topCornerRight.getX(), _topCornerRight.getY(), BLACK, 100.0f).display(disp);
}

myShapes::Rectangle::Rectangle(const Point & a, double length, double width, const string & type, const string & name) : Polygon(type, name)
{
	this->_topCornerLeft = a;
	this->_topCornerRight.setX(a.getX() + length);
	this->_bottomCornerLeft.setY(a.getY() - width);

	this->_bottomCornerRight.setY(a.getY() - width);
	this->_bottomCornerRight.setY(a.getX() + length);
	
	this->_len = length;
	this->_width = width;


}

myShapes::Rectangle::~Rectangle()
{
}

double myShapes::Rectangle::getArea() const
{
	return this->_len * this->_width;
}

double myShapes::Rectangle::getPerimeter() const
{
	return this->_len * 2 + this->_width * 2;
}

void myShapes::Rectangle::move(const Point & other)
{
	this->_topCornerLeft += other;
	this->_topCornerRight += other;
	this->_bottomCornerLeft += other;
	this->_bottomCornerRight += other;
}
